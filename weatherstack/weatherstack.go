package weatherstack

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/daafuku/zai-coding-test/weather"
)

type Config struct {
	AccessKey string `toml:"access_key"`
}

type WeatherProvider struct {
	accessKey string
}

func New(config Config) *WeatherProvider {
	return &WeatherProvider{
		accessKey: config.AccessKey,
	}
}

type WeatherstackError struct {
	Code int    `json:"code"`
	Type string `json:"type"`
	Info string `json:"info"`
}

type response struct {
	Error   *WeatherstackError `json:"error"`
	Current struct {
		WindSpeed          *int `json:"wind_speed"`
		TemperatureDegrees *int `json:"temperature"`
	} `json:"current"`
}

func (p *WeatherProvider) Weather(ctx context.Context, city string) (report weather.Report, err error) {
	// http://api.weatherstack.com/current
	// ? access_key = YOUR_ACCESS_KEY
	// & query = New York

	requestUrl, err := url.Parse("http://api.weatherstack.com/current")
	if err != nil {
		panic(err)
	}

	query := url.Values{
		"access_key": {p.accessKey},
		"query":      {city},
	}

	requestUrl.RawQuery = query.Encode()

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		requestUrl.String(),
		nil,
	)
	if err != nil {
		err = fmt.Errorf("weatherstack: failed to build request: %w", err)
	}

	// improvement: could supply client through context
	httpRes, err := http.DefaultClient.Do(req)
	if err != nil {
		err = fmt.Errorf("weatherstack: failed to fetch results: %w", err)
		return
	}
	defer httpRes.Body.Close()

	if httpRes.StatusCode != 200 {
		err = fmt.Errorf("weatherstack: api server returned %d", httpRes.StatusCode)
		return
	}

	var res response

	res.Current.TemperatureDegrees = &report.TemperatureDegrees
	res.Current.WindSpeed = &report.WindSpeed

	if err = json.NewDecoder(httpRes.Body).Decode(&res); err != nil {
		err = fmt.Errorf("weatherstack: failed to decode response: %w", err)
		return
	}

	// improvement: could try to ascertain whether the error was due to a bad input
	// to this method (400), a transient server failure (5XX)
	if res.Error != nil {
		err = fmt.Errorf("weatherstack: API error (%d): %s", res.Error.Code, res.Error.Info)
		return
	}

	return
}
