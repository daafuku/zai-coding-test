package failover

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/daafuku/zai-coding-test/weather"
)

type Config struct {
	Timeout time.Duration `toml:"timeout"`
}

// failover.Provider queries it's sources for a weather report, it sets a timeout for each individual
// provider
type Provider struct {
	sources []weather.Provider
	timeout time.Duration
}

func New(config Config, sources []weather.Provider) *Provider {
	p := &Provider{
		timeout: config.Timeout,
		sources: sources,
	}

	return p
}

func (p *Provider) Weather(ctx context.Context, city string) (report weather.Report, err error) {
	for _, source := range p.sources {
		ctx, cancel := context.WithTimeout(ctx, p.timeout)
		defer cancel()
		if report, err = source.Weather(ctx, city); err != nil {
			log.Printf("failover: failed to fetch weather report from source: %s", err)
			continue
		} else {
			return
		}
	}

	err = fmt.Errorf("failover: failed to source weather report from any providers")
	return
}
