zai-coding-test
===

Written in a few hours for the Zai/Assembly Payments coding test.

For a quick preview; check out https://darfk.net/v1/weather?city=melbourne .

Addressing the specifications
---

> The service can hard-code Melbourne as a city.

The query param `city` is validated at the start of each request

> The service should return a JSON payload with a unified response containing temperature in degrees Celsius and wind speed.

Done

> If one of the providers goes down, your service can quickly failover to a different provider without affecting your customers.

This is achieved with `failover.Provider` which iterates through it's sub-providers implementing the `weather.Provider` interface until it recieves a non erroneous response. Each sub-provider is given a configurable duration before it's context is cancelled.

> Have scalability and reliability in mind when designing the solution.

Providers can easily be added and caching/failover can easily be extended.

> Weather results are fine to be cached for up to 3 seconds on the server in normal behaviour to prevent hitting weather providers.

> Cached results should be served if all weather providers are down.

This is achieved with a `cached.Provider` which is a weather provider that returns a valid cached report, falling back to calling it's sub-provider for a realtime value. If the sub-provider fails then the the `cached.Provider` returns it's last cached report and an error to signal that the caller should expect a expired weather report. If no report was ever cached then the cache returns an error.

> The proposed solution should allow new developers to make changes to the code safely.

I hope that the modular approach I took will make the application easily extensible.

Installation and Building
---

First download the package source and change into the directory

```
go get gitlab.com/daafuku/zai-coding-test
cd $GOPATH/src/gitlab.com/daafuku/zai-coding-test
```

The following should get you up and running to host the server on your local machine.

```
go build -o server ./cmd/server
cp config.sample.toml config.toml
# edit your config.toml file ...
./server
```

If you'd prefer to containerise things then I've included a docker file.

```
docker build -t thomas-payne-zai-coding-test .
docker run \
 -p 8014:8014 \
 -e WEATHERSTACK_ACCESS_KEY=<your weatherstack access key> \
 -e OPENWEATHER_API_KEY=<your openweather API key> \
  thomas-payne-zai-coding-test
```

Addressing the 'What we are looking for' section
---

> Test coverage and approach to writing tests

- Test coverage for some packages is close to 100%
- I ran out of time and did not write tests for the `failover` package

> Ease of running code locally

Detailed above, portability is at the forefront of my development philosophies, two different options exist for ease of running the code.

>  Trade-offs you might have made, anything you left out, or what you might do differently if you were to spend additional time on the task.

- I should have used a configuration manager.
- Instead of writing a server in `server/server.go`, I probably could have just implemented `http.ServeMux`.
- I should have used a `sync.RWMutex` instead of just a `sync.Mutex` to lock the cache. Better still I could have used an existing cache implementation.
- I could have fanned out the request over all the providers and serve up the first one to return successful. Instead I opted to try one at a time.

- Throughout the code there are `improvement:` notes.

Other notes
---

~~This service has yet to be hosted, it may become available shortly.~~ 2021-11-13: This service is currently being hosted at https://darfk.net/v1/weather?city=melbourne

This task was pretty fun but relatively easy.
