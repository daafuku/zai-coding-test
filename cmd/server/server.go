package main

import (
	"log"
	"os"
	"time"

	"github.com/pelletier/go-toml"
	"gitlab.com/daafuku/zai-coding-test/cached"
	"gitlab.com/daafuku/zai-coding-test/failover"
	"gitlab.com/daafuku/zai-coding-test/openweather"
	"gitlab.com/daafuku/zai-coding-test/server"
	"gitlab.com/daafuku/zai-coding-test/weather"
	"gitlab.com/daafuku/zai-coding-test/weatherstack"
)

type Config struct {
	Env          string              `toml:"env"`
	Server       server.Config       `toml:"server"`
	Weatherstack weatherstack.Config `toml:"weatherstack"`
	Openweather  openweather.Config  `toml:"openweather"`
	Failover     failover.Config     `toml:"failover"`
	Cache        cached.Config       `toml:"cache"`
}

func main() {
	var config Config

	if fd, err := os.Open("config.toml"); err != nil {
		log.Printf("failed to load config file: %s", err)
		return
	} else {
		err = toml.NewDecoder(fd).Decode(&config)
		fd.Close()
		if err != nil {
			log.Printf("failed to parse config file: %s", err)
			return
		}
	}

	if v := os.Getenv("WEATHERSTACK_ACCESS_KEY"); v != "" {
		config.Weatherstack.AccessKey = v
	}

	if v := os.Getenv("OPENWEATHER_API_KEY"); v != "" {
		config.Openweather.APIKey = v
	}

	var providers []weather.Provider

	if config.Env == "production" {
		providers = []weather.Provider{
			weatherstack.New(config.Weatherstack),
			openweather.New(config.Openweather),
		}
	} else {
		providers = []weather.Provider{
			&weather.DummyWeatherProvider{
				Delay:        time.Millisecond * 250,
				StaticReport: weather.Report{WindSpeed: 29, TemperatureDegrees: 20},
			},
		}
	}

	server := server.New(
		config.Server,
		cached.New(
			config.Cache,
			failover.New(
				config.Failover,
				providers,
			),
		),
	)

	log.Fatal(server.Start())
}
