package cached

import (
	"context"
	"fmt"
	"testing"
	"time"

	"gitlab.com/daafuku/zai-coding-test/weather"
)

type shouldUseCacheWeatherProvider struct {
	t *testing.T
	i int
}

func (p *shouldUseCacheWeatherProvider) Weather(ctx context.Context, _ string) (weather.Report, error) {
	if p.i != 0 {
		p.t.Error("weather provider was called twice, last result should have been cached")
	}

	p.i++

	return weather.Report{}, nil
}

func TestCache(t *testing.T) {

	p := New(
		Config{ValidFor: time.Duration(time.Second)},
		&shouldUseCacheWeatherProvider{t: t},
	)

	p.Weather(context.TODO(), "test")
	p.Weather(context.TODO(), "test")
}

type onlyErrorsWeatherProvider struct{}

func (p *onlyErrorsWeatherProvider) Weather(ctx context.Context, _ string) (weather.Report, error) {
	return weather.Report{}, fmt.Errorf("cannot provide weather")
}

func TestCacheDry(t *testing.T) {
	p := New(
		Config{ValidFor: time.Duration(time.Second)},
		&onlyErrorsWeatherProvider{},
	)

	_, err := p.Weather(context.TODO(), "test")

	if err == nil {
		t.Error("cache should have errored as it has no stored values")
	}

}
