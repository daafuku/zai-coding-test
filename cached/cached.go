package cached

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"gitlab.com/daafuku/zai-coding-test/weather"
)

type Config struct {
	ValidFor time.Duration `toml:"valid_for"`
}

var ErrExpiredReport = errors.New("returned report has expired")

type cacheEntry struct {
	stored  weather.Report
	fetched time.Time
	expires time.Time
}

// cache.Provider stores a single report for a city and returns it until it has expired,
// after which it will request a report from the report source
type Provider struct {
	// validFor determines the duration of the validity of a cached report
	validFor time.Duration
	// source is the weather.Provider that will be used if a valid cached report is not present
	source weather.Provider

	mutex sync.Mutex

	entries map[string]*cacheEntry
}

func New(config Config, source weather.Provider) *Provider {
	if source == nil {
		panic("cached: source is nil")
	}

	p := &Provider{
		validFor: config.ValidFor,
		source:   source,
	}

	return p
}

func (p *Provider) Weather(ctx context.Context, city string) (report weather.Report, err error) {
	// improvement: a more complex locking mechanism could be used
	p.mutex.Lock()
	defer p.mutex.Unlock()

	if p.entries == nil {
		p.entries = make(map[string]*cacheEntry)
	}

	// no entry for this city exists?
	entry, exists := p.entries[city]
	if !exists {
		entry = &cacheEntry{}
		p.entries[city] = entry
	}

	// if the request is made before the stored report has expired then return the stored report
	if time.Now().Before(entry.expires) {
		report = entry.stored
		return
	}

	// try to fetch another report from the source
	report, err = p.source.Weather(ctx, city)
	if err != nil {
		// if we cannot fetch it and we have no stored report to fall back on then
		// we cannot do anything and must raise an error
		if entry.fetched.IsZero() {
			err = fmt.Errorf("cache: failed to fetch report from source and there is no cached report: %w", err)
			return
		}

		// return the stored report but also raise an error to let the caller know
		// if they want to use an out of date report
		report = entry.stored
		err = ErrExpiredReport
		return
	}

	// update the fetched and expiry times
	entry.fetched = time.Now()
	entry.expires = entry.fetched.Add(p.validFor)
	entry.stored = report
	return
}
