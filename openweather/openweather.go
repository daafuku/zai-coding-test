package openweather

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/daafuku/zai-coding-test/weather"
)

type Config struct {
	APIKey string `toml:"api_key"`
}

type Provider struct {
	apiKey string
}

func New(config Config) *Provider {
	return &Provider{
		apiKey: config.APIKey,
	}
}

type response struct {
	Main struct {
		Temperature json.Number `json:"temp"`
	} `json:"main"`
	Wind struct {
		Speed json.Number `json:"speed"`
	} `json:"wind"`
}

func (p *Provider) Weather(ctx context.Context, city string) (report weather.Report, err error) {
	// http://api.openweathermap.org/data/2.5/weather?q=melbourne,AU&appid=abc123

	requestUrl, err := url.Parse("http://api.openweathermap.org/data/2.5/weather")
	if err != nil {
		panic(err)
	}

	// since we're restricting the city to melbourne then we
	// can hardcode the country
	// needs to be removed if this is ever used
	city = strings.Join([]string{city, "AU"}, ",")

	query := url.Values{
		"appid": {p.apiKey},
		"q":     {city},
		// openweather units default to "standard" and returns temperatures in Kelvin
		"units": {"metric"},
	}

	requestUrl.RawQuery = query.Encode()

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		requestUrl.String(),
		nil,
	)
	if err != nil {
		err = fmt.Errorf("openweather: failed to build request: %w", err)
	}

	// improvement: could supply client through context
	httpRes, err := http.DefaultClient.Do(req)
	if err != nil {
		err = fmt.Errorf("openweather: failed to fetch results: %w", err)
		return
	}
	defer httpRes.Body.Close()

	if httpRes.StatusCode != 200 {
		err = fmt.Errorf("openweather: api server returned %d", httpRes.StatusCode)
		return
	}

	var res response

	if err = json.NewDecoder(httpRes.Body).Decode(&res); err != nil {
		err = fmt.Errorf("openweather: failed to decode response: %w", err)
		return
	}

	// I know this looks funky, but roll with me here. This was I don't have to rewrite the
	// json.Number -> int conversion
	for _, s := range []struct {
		src  json.Number
		dst  *int
		name string
	}{
		{res.Main.Temperature, &report.TemperatureDegrees, "temperature"},
		{res.Wind.Speed, &report.WindSpeed, "wind speed"},
	} {
		var f float64
		f, err = s.src.Float64()
		if err != nil {
			err = fmt.Errorf("openweather: failed to parse '%s' as float: %w", s.name, err)
			return
		}

		// improvement: should do some work to make sure that f
		// does not exceed int representable value
		*s.dst = int(math.Round(f))
	}

	return
}
