package server

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/daafuku/zai-coding-test/weather"
)

type Config struct {
	Addr string `toml:"addr"`
}

type Server struct {
	weatherProvider weather.Provider
	server          *http.Server
}

func (s *Server) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	if req.URL.Path != "/v1/weather" {
		http.NotFound(res, req)
		return
	}

	city := req.URL.Query().Get("city")

	if err := weather.ValidateCity(city); err != nil {
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte(err.Error()))
		return
	}

	report, err := s.weatherProvider.Weather(req.Context(), city)
	if err != nil {
		res.WriteHeader(http.StatusBadGateway)
		res.Write([]byte(err.Error()))
		return
	}

	res.Header().Add("Content-Type", "application/json")
	res.WriteHeader(http.StatusOK)

	err = json.NewEncoder(res).Encode(report)
	if err != nil {
		log.Printf("server: failed to write response: %s", err)
	}
}

func New(config Config, source weather.Provider) (server *Server) {
	server = &Server{}

	server.server = &http.Server{
		Addr:    config.Addr,
		Handler: server,
	}

	server.weatherProvider = source

	return
}

func (server *Server) Start() error {
	return server.server.ListenAndServe()
}
