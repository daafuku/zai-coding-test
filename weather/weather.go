package weather

import (
	"context"
	"errors"
)

var ErrInvalidCity = errors.New("invalid city, only 'melbourne' is allowed")

type Report struct {
	WindSpeed          int `json:"wind_speed"`
	TemperatureDegrees int `json:"temperature_degrees"`
}

func ValidateCity(city string) error {
	if city != "melbourne" {
		return ErrInvalidCity
	}

	return nil
}

type Provider interface {
	Weather(ctx context.Context, city string) (report Report, err error)
}
