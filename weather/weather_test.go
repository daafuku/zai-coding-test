package weather

import (
	"context"
	"errors"
	"testing"
	"time"
)

func TestDummyError(t *testing.T) {
	d := DummyWeatherProvider{
		StaticReport: Report{
			20, 29,
		},
	}

	_, err := d.Weather(context.TODO(), "sydney")

	if err == nil {
		t.Error("failed to provide an error on unknown city")
	}

	if !errors.Is(ErrInvalidCity, err) {
		t.Error("failed to provide a ErrCityDoesNotExist error on unknown city")
	}

	report, err := d.Weather(context.TODO(), "melbourne")

	if err != nil {
		t.Errorf("error when reporting weather: %e", err)
	}

	t.Logf("reported weather: %+v", report)
}

func TestDummyDelay(t *testing.T) {
	// simulate a timeout
	ctx, cancel := context.WithTimeout(context.Background(), 0)

	d := DummyWeatherProvider{
		time.Second,
		Report{
			20, 29,
		},
	}

	_, err := d.Weather(ctx, "melbourne")

	if err == nil {
		t.Error("failed to provide error when service was intentionally cancelled")
	} else {
		t.Log(err)
	}

	cancel()
}
