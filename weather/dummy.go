package weather

import (
	"context"
	"fmt"
	"time"
)

type DummyWeatherProvider struct {
	Delay        time.Duration
	StaticReport Report
}

func (p *DummyWeatherProvider) Weather(ctx context.Context, city string) (report Report, err error) {
	if p.StaticReport.TemperatureDegrees == 0 || p.StaticReport.WindSpeed == 0 {
		err = fmt.Errorf("dummy: static report not configured")
		return
	}

	// inject some dummy delay to simulate a web request
	timer := time.NewTimer(p.Delay)

	// if the timer fires first serve the request with no error
	// if the context is cancelled then stop the timer, drain the timer chan
	// and raise an error
	select {
	case <-timer.C:
	case <-ctx.Done():
		err = ctx.Err()
		if !timer.Stop() {
			<-timer.C
		}
	}

	if err != nil {
		return
	}

	report.TemperatureDegrees = 20
	report.WindSpeed = 29

	return
}
